(ns datomic-demo.entry
  "Documents scenario"
  (:require [clojure.core :refer :all]
            [datomic.client.api :as d]
            [datomic-demo.schema]
            [java-time :refer [java-date
                               instant
                               zoned-date-time
                               minus
                               local-date-time
                               minutes]]))

(def config {:server-type :peer-server
             :access-key "myaccesskey"
             :secret "mysecret"
             :endpoint "127.0.0.1:8998"
             :validate-hostnames false})

(def client (d/client config))

(def conn (d/connect client {:db-name "hello"}))

; put 9 commands in 1 transaction
(d/transact conn {:tx-data datomic-demo.schema/attributes})














;; Erik uploads
(def game-of-thrones-finale
   [{:db/id "erik"
     :hyves.user/id (java.util.UUID/randomUUID)
     :user/email-address "erik@evanv.nl"}

    {:db/id "daniel"
     :person/name "Daniel Brett Weiss"
     :person/year-of-birth 1971}

    {:db/id "david"
     :person/name "David Benioff"
     :person/year-of-birth 1970}

    {:db/id "document"
     :document/file-name  "game-of-thrones.s8e6.mkv"
     :document/uploader   "erik"
     :document/authors    ["david" "daniel"]
     :document/purposes   [:entertainment] ;; brackets optional
     :amazon.s3/object-id "VYcoehXqpyk_game-of-thrones.s8e6.mkv"}])

(d/transact conn {:tx-data game-of-thrones-finale})

; datom = EAVTO
; E = Entity id
; A = Attribute id
; V = Value
; T = Transaction id
; O = Operation (add or retract)

;; fetch current database value for the first time
(def db (d/db conn))

;; could have made it easier on myself by using :db/ident
(d/pull db '[*] 17592186045421)
(d/pull db '[*] 79)

(d/pull db
        ['*
         {:document/uploader '[*]
          :document/authors  '[*]}]
        17592186045421)

(d/pull db '[*] [:user/email-address "erik@evanv.nl"])













;; Jan Jaap updates
(def game-of-thrones-finale-v2
  [{:db/id "peter"
    :person/name "Peter Jackson"
    :person/year-of-birth 1961}

   {:db/id "janjaap"
    :user/email-address "janjaap@comp.os.bsd.org"}

   {:db/id 17592186045421
    ;; could change the file name too
    :document/authors  ["peter"]
    :document/uploader "janjaap"
    :amazon.s3/object-id "Zk2CsYril44_game-of-thrones.s8e6.mkv"}

   [:db/retract 17592186045421 :document/authors 17592186045419]
   [:db/retract 17592186045421 :document/authors 17592186045420]])

(d/transact conn {:tx-data game-of-thrones-finale-v2})

;; same query as before
(d/pull db
        ['*
         {:document/uploader '[*]
          :document/authors  '[*]}]
        17592186045421)

(def db2 (d/db conn))

(d/pull db2
        ['*
         {:document/uploader '[*]
          :document/authors  '[*]}]
        17592186045421)

(def old-db (d/as-of db 13194139534313))
(def old-db (d/as-of db (java-date (minus (zoned-date-time) (minutes 5)))))










;; Nicky wants to revert
(d/q '[:find (max ?tx)
       :keys last-tx ;; optional alias
       :in $ ?e
       :where [?e _ _ ?tx]]
     db2 17592186045421)

(def datoms (d/q '[:find ?e ?a ?v ?tx ?op
                   :in $ ?e ?tx
                   :where [?e ?a ?v ?tx ?op]
                          ;; exclude the previous uploader
                          ;; would cause a cardinality violation
                          (not [?a :db/ident :document/uploader])
                   ]
               (d/history db2)
               17592186045421
               13194139534318))

(defn datom-to-inverse-operation [[e a v _ op]]
  [(if op :db/retract :db/add) e a v])

(def inverse-transactions (map datom-to-inverse-operation datoms))

(def revert-got-remake-transaction
   (conj inverse-transactions
         {:db/id "nicky"
          :user/email-address "nicky@pikachu.vcn.nl"}
         {:db/id 17592186045421
          :document/uploader "nicky"}))

(d/transact conn {:tx-data revert-got-remake-transaction})

(d/pull (d/db conn)
        ['*
         {:document/uploader '[*]
          :document/authors  '[*]}]
        17592186045421)







;; which uploaders like which authors?
(d/q '[:find ?uploader-email-address ?author-name
       :keys uploader author
       :where [?document :document/uploader  ?uploader ?tx true]
              [?document :document/authors   ?author   ?tx true]
              [?uploader :user/email-address ?uploader-email-address]
              [?author   :person/name        ?author-name]]
     (d/history (d/db conn)))












;; find e-mail addresses by tld
(d/q '[:find ?email-address
       :keys email-address
       :in $ ?tld
       :where [_ :user/email-address ?email-address]
               ;; Java interop
               [(.endsWith ?email-address ?tld)]]
     (d/db conn)
     ".nl")
