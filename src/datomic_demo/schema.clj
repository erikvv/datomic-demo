(ns datomic-demo.schema)

(def attributes
  [
   ;; User attributes

   {:db/ident       :hyves.user/id
    :db/valueType   :db.type/uuid
    :db/cardinality :db.cardinality/one
    :db/unique      :db.unique/identity
    :db/doc         "User id in external system"}

   {:db/ident       :user/email-address
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one
    :db/unique      :db.unique/identity
    :db/doc         "User's e-mail address"}

   ;; Author/Person attributes

   {:db/ident       :person/name
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc         "Persons full name"}

   {:db/ident       :person/year-of-birth
    :db/valueType   :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc         "Persons year of birth"}

   ;; Document attributes

   {:db/ident       :document/file-name
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc         "File name including extension"}

   {:db/ident       :amazon.s3/object-id
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one
    :db/unique      :db.unique/identity
    :db/doc         "Document id in external system"}

   {:db/ident       :document/purposes
    :db/valueType   :db.type/keyword
    :db/cardinality :db.cardinality/many
    :db/doc         "Contexts in which this document should be available"}

   {:db/ident       :document/uploader
    :db/valueType   :db.type/ref
    :db/cardinality :db.cardinality/one
    :db/doc         "User who uploaded this document"}

   {:db/ident       :document/authors
    :db/valueType   :db.type/ref
    :db/cardinality :db.cardinality/many
    :db/doc         "Original authors of the document"}])
