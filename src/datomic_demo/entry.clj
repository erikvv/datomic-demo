(ns datomic-demo.entry
  "Entry point"
  (:require [clojure.core :refer :all]
            [datomic.client.api :as d]
            [datomic-demo.schema]
            [clojure.tools.nrepl.server]
            [java-time :refer [java-date
                               instant
                               zoned-date-time
                               minus
                               local-date-time
                               minutes]]))

;(future (clojure.tools.nrepl.server/start-server :port 7888))

;(require '[datomic-demo.documents])
;(in-ns


;
;;Add user email addresses to database
;(def add-emails
;  [{:db/id #db/id[:db.part/user -1000001] :person/email "example@gmail.com"}
;   {:db/id #db/id[:db.part/user -1000002] :person/email "john@gmail.com"}
;   {:db/id #db/id[:db.part/user -1000003] :person/email "jane@gmail.com"}])
;
;(d/transact conn add-emails)