
var userIdAttribute = {
    "db/ident":       "insurance-company/code",
    "db/valueType":   "db.type/string",
    "db/cardinality": "db.cardinality/one",
    "db/doc":         "Insurance company code",
};

// or maybe more accurately
var userIdAttribute = {
    [Symbol.for("db/ident")]:       Symbol.for("insurance-company/code"),
    [Symbol.for("db/valueType")]:   Symbol.for("db.type/string"),
    [Symbol.for("db/cardinality")]: Symbol.for("db.cardinality/one"),
    [Symbol.for("db/doc")]:         "Insurance company code",
};